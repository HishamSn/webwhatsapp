package com.zero1tech.whatsappweb.util;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Message;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static com.zero1tech.whatsappweb.constant.AppConstant.AUDIO_PERMISSION;
import static com.zero1tech.whatsappweb.constant.AppConstant.AUDIO_PERMISSION_RESULTCODE;
import static com.zero1tech.whatsappweb.constant.AppConstant.CAMERA_PERMISSION;
import static com.zero1tech.whatsappweb.constant.AppConstant.CAMERA_PERMISSION_RESULTCODE;
import static com.zero1tech.whatsappweb.constant.AppConstant.DEBUG_TAG;
import static com.zero1tech.whatsappweb.constant.AppConstant.FILECHOOSER_RESULTCODE;
import static com.zero1tech.whatsappweb.constant.AppConstant.VIDEO_PERMISSION;
import static com.zero1tech.whatsappweb.constant.AppConstant.VIDEO_PERMISSION_RESULTCODE;

public class CustomWebChromeClient extends WebChromeClient {

    private Activity activity;
    private PermissionRequest permissionRequest;
    private ValueCallback<Uri[]> mUploadMessage;

    public CustomWebChromeClient(Activity activity) {
        this.activity = activity;
    }


    @Override
    public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg) {
        Toast.makeText(activity, "OnCreateWindow", Toast.LENGTH_LONG).show();
        return true;
    }

    public PermissionRequest getPermissionRequest() {
        return permissionRequest;
    }

    public ValueCallback<Uri[]> getUploadMessage() {
        return mUploadMessage;
    }

    public void setPermissionRequest(PermissionRequest permissionRequest) {
        this.permissionRequest = permissionRequest;
    }

    @Override
    public void onPermissionRequest(final PermissionRequest request) {
        if (request.getResources()[0].equals(PermissionRequest.RESOURCE_VIDEO_CAPTURE)) {
            if (ContextCompat.checkSelfPermission(activity, CAMERA_PERMISSION) == PackageManager.PERMISSION_DENIED
                    && ContextCompat.checkSelfPermission(activity, AUDIO_PERMISSION) == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(activity, VIDEO_PERMISSION, VIDEO_PERMISSION_RESULTCODE);
                permissionRequest = request;
            } else if (ContextCompat.checkSelfPermission(activity, CAMERA_PERMISSION) == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(activity, new String[]{CAMERA_PERMISSION}, CAMERA_PERMISSION_RESULTCODE);
                permissionRequest = request;
            } else if (ContextCompat.checkSelfPermission(activity, AUDIO_PERMISSION) == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(activity, new String[]{AUDIO_PERMISSION}, AUDIO_PERMISSION_RESULTCODE);
                permissionRequest = request;
            } else {
                request.grant(request.getResources());
            }
        } else if (request.getResources()[0].equals(PermissionRequest.RESOURCE_AUDIO_CAPTURE)) {
            if (ContextCompat.checkSelfPermission(activity, AUDIO_PERMISSION) == PackageManager.PERMISSION_GRANTED) {
                request.grant(request.getResources());
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{AUDIO_PERMISSION}, AUDIO_PERMISSION_RESULTCODE);
                permissionRequest = request;
            }
        } else {
            try {
                request.grant(request.getResources());
            } catch (RuntimeException e) {
                Log.d(DEBUG_TAG, "Granting permissions failed", e);
            }
        }
    }

    public boolean onConsoleMessage(ConsoleMessage cm) {
        Log.d(DEBUG_TAG, "WebView console message: " + cm.message());
        return super.onConsoleMessage(cm);
    }

    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
        mUploadMessage = filePathCallback;
        Intent chooserIntent = fileChooserParams.createIntent();
        activity.startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
        return true;
    }

}
