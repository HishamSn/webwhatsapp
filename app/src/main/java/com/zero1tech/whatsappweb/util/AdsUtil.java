package com.zero1tech.whatsappweb.util;

import android.app.Activity;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.zero1tech.whatsappweb.model.ErrorAds;
import com.zero1tech.whatsappweb.repo.AdsUserRepo;

import java.util.Date;

import static com.zero1tech.whatsappweb.util.FirebaseAnalyticsUtil.logEventWithItemId;

public class AdsUtil {
    private static AdView adViewGoogle;


//    public static InterstitialAd initInterstitialAd(Context context, InterstitialAd ad) {
//        ad = new InterstitialAd(context);
//        ad.setAdUnitId(BuildConfig.MAIN_NATIVE_ID);
//        ad.loadAd(new AdRequest.Builder().build());
//        return ad;
//    }

    public static AdView initBannerAd(AdView ad, LinearLayout containerAdView, String unit) {
//        ad = new AdView(context);
        ad.setAdUnitId(unit);
        ad.setAdSize(AdSize.BANNER);
        containerAdView.addView(ad);
        AdRequest adRequest = new AdRequest.Builder().build();
        ad.loadAd(adRequest);
        ad.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                logEventWithItemId("AD_ON_Loaded");


            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                Date date = new Date();
                String dateFormat = DateUtil.getSimpleDateFormatTime().format(date);


                logEventWithItemId("AD_FAIL_LOAD");

                ErrorAds errorAds = new ErrorAds();
                errorAds.setCreatedAt(dateFormat);
                errorAds.setDomain(adError.getDomain());
                errorAds.setMessage(adError.getMessage());
                errorAds.setResponseInfo(adError.getResponseInfo().toString());
                errorAds.setCode(adError.getCode());
                AdsUserRepo.getInstance().insertErrorAdsActivity(errorAds, success -> {
                    logEventWithItemId("AD_FAIL_REPORTED");
                });
                Log.d(AdsUtil.class.getSimpleName(), "AD_FAIL_LOAD");

                if (errorAds.getCode() == 0) {
                    Log.d(AdsUtil.class.getSimpleName(), "No_Internet");
                }


            }

            @Override
            public void onAdOpened() {
                logEventWithItemId("AD_ON_OPENED");

            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
//                Bundle bundle = new Bundle();
//                bundle.putString("email", email);
//                MyApplication.getInstance().getFirebaseAnalytics().logEvent("try_login", bundle);
                logEventWithItemId("AD_CLICKED");


            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                logEventWithItemId("onAdLeftApplication");

            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
                logEventWithItemId("onAdClosed");

            }
        });


        return ad;
    }


    public static void BannerAds(Activity activity, AdView adView, CardView cardView) {
        adViewGoogle = adView;
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        adView.loadAd(adRequest);


    }

}

