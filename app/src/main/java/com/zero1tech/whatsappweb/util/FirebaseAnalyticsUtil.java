package com.zero1tech.whatsappweb.util;

import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.zero1tech.whatsappweb.common.MyApplication;
import com.zero1tech.whatsappweb.data.prefs.PrefsUtils;

public class FirebaseAnalyticsUtil {


    public static void logEventWithItemId(String eventKey) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, PrefsUtils.getInstance().getSerialDevice());
        MyApplication.getInstance().getFirebaseAnalytics().logEvent(eventKey, bundle);
    }

    public static void logEvent(String eventKey, Bundle bundle) {

        MyApplication.getInstance().getFirebaseAnalytics().logEvent(eventKey, bundle);

    }


    public static void logEvent(String eventKey) {

        MyApplication.getInstance().getFirebaseAnalytics().logEvent(eventKey, null);

    }

    public static void logEvent(String eventKey,String paramId, String paramValue) {
        Bundle bundle = new Bundle();
        bundle.putString(paramId, paramValue);
        MyApplication.getInstance().getFirebaseAnalytics().logEvent(eventKey, bundle);

    }

}
