package com.zero1tech.whatsappweb.util;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.zero1tech.whatsappweb.R;


public class NotificationUpdate extends ContextWrapper {
    public static final String channelID = "channel_id_alamar";
    public static final String channelName = "Channel_alamar";

    private NotificationManager mManager;

    public NotificationUpdate(Context context) {
        super(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel() {
        NotificationChannel channel = new NotificationChannel(channelID, channelName, NotificationManager.IMPORTANCE_HIGH);

        getManager().createNotificationChannel(channel);
    }

    public NotificationManager getManager() {
        if (mManager == null) {
            mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }

        return mManager;
    }

    public NotificationCompat.Builder getChannelNotification() {

//        Intent updateIntent = new Intent(Intent.ACTION_VIEW);
//        updateIntent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + "com.amsy.f_athkar"));
//
//        PendingIntent toActivityPeindingIntent = PendingIntent.getActivity(this, 0,updateIntent , PendingIntent.FLAG_UPDATE_CURRENT);
        return new NotificationCompat.Builder(getApplicationContext(), channelID)
                .setContentTitle("Please update the app")
                .setContentText("New update .. new features are waiting for you")
                .setSmallIcon(R.drawable.ic_logo)
                .setAutoCancel(true);
//                .setContentIntent(toActivityPeindingIntent);

    }
}

