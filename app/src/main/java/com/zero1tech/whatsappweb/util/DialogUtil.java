package com.zero1tech.whatsappweb.util;

import android.content.Context;
import android.graphics.Color;

import com.zero1tech.whatsappweb.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static cn.pedant.SweetAlert.SweetAlertDialog.PROGRESS_TYPE;


public final class DialogUtil {



    public static SweetAlertDialog progress(Context context) {
        SweetAlertDialog pDialog = new SweetAlertDialog(context, PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#000080"));
        pDialog.setTitleText(R.string.loading);
        pDialog.setCancelable(false);
        return pDialog;
    }


}
