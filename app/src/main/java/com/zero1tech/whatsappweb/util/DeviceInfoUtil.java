package com.zero1tech.whatsappweb.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings;

import com.google.firebase.crashlytics.FirebaseCrashlytics;

public class DeviceInfoUtil {

    @SuppressLint("HardwareIds")
    public static String getAndroidId(Context context) {
        String androidId;
        try {
            androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            if (androidId == null) {
                throw new NullPointerException();
            }

        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
            androidId = SerialDeviceUtil.getSerialNumber();
        }
        return androidId;
    }
}
