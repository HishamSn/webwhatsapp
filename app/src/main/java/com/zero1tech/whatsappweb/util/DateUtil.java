package com.zero1tech.whatsappweb.util;

import java.text.SimpleDateFormat;
import java.util.Locale;

public final class DateUtil {
    private final static String dateFormat = "yyyy-MM-dd"; //example date: "2014-02-27T10"
    private final static String dateFormatTime = "yyyy-MM-dd hh:mm:ss"; //example date: "2014-02-27T10"
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
    private static SimpleDateFormat simpleDateFormatTime = new SimpleDateFormat(dateFormatTime, Locale.ENGLISH);

//


    public static String getDefaultDateFormat() {
        return dateFormat;
    }
    public static String getDateFormatTime() {
        return dateFormatTime;
    }

    public static SimpleDateFormat getSimpleDateFormat() {
        return simpleDateFormat;
    }

    public static SimpleDateFormat getSimpleDateFormatTime() {
        return simpleDateFormatTime;
    }

}
