package com.zero1tech.whatsappweb.util;

import android.app.ProgressDialog;
import android.content.Context;

import com.zero1tech.whatsappweb.BuildConfig;
import com.zero1tech.whatsappweb.R;
import com.zero1tech.whatsappweb.data.prefs.PrefsUtils;

import static com.zero1tech.whatsappweb.util.Dialog.showDialogInfo;

public class Utils {

    public static void showUpdateVersionSuccess(Context context) {
        int lastShownVersionCode = PrefsUtils.getInstance().getLastShownVersionCode();
        int currentVersionCode = BuildConfig.VERSION_CODE;

        if (lastShownVersionCode == 0) {
            PrefsUtils.getInstance().setLastShownVersionCode(currentVersionCode);
            return;
        }
        if (lastShownVersionCode < currentVersionCode) {
            showDialogInfo(context, R.string.versionInfo);
        } else {
            return;
        }
        PrefsUtils.getInstance().setLastShownVersionCode(currentVersionCode);
    }

    public static void showIntroInfo(Context context) {
        if (!PrefsUtils.getInstance().isIntroShow()) {
            showDialogInfo(context, R.string.introInfo);
        } else {
            return;
        }
        PrefsUtils.getInstance().setIntroShow(true);
    }
    public static ProgressDialog progressUtil(Context context) {
        ProgressDialog progress = new ProgressDialog(context);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog

        return progress;
//        progress.show();
// To dismiss the dialog
//        progress.dismiss();
    }

}
