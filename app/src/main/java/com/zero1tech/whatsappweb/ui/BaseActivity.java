package com.zero1tech.whatsappweb.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.graphics.Color;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.zero1tech.whatsappweb.BuildConfig;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {


    public void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    public void showSnackbar(String msg) {
        final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), msg, 900);
        snackbar.setAction("dismiss", (View view) -> snackbar.dismiss());
        snackbar.setActionTextColor(Color.parseColor("#075E54"));
    }

    private void showPopupDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", null);
        AlertDialog alert = builder.create();
        alert.show();
    }


    public Boolean isReleaseEnv() {
        return BuildConfig.BUILD_TYPE.equals("release");
    }


//    @Override
//    public void onBackPressed() {
//        //close drawer if open and impl. press back again to leave
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else if (System.currentTimeMillis() - mLastBackClick < 1100) {
//            finishAffinity();
//        } else {
//            mWebView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ESCAPE));
//            showToast("Click back again to close");
//            mLastBackClick = System.currentTimeMillis();
//        }
//    }

}
