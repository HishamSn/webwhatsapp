package com.zero1tech.whatsappweb.ui.setting;

import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.zero1tech.whatsappweb.R;
import com.zero1tech.whatsappweb.data.prefs.PrefsUtils;

import static com.zero1tech.whatsappweb.constant.FirebaseConstant.ACTIVITY_MAINTENANCE;
import static com.zero1tech.whatsappweb.constant.FirebaseConstant.PARAM_USER_DEVICE;
import static com.zero1tech.whatsappweb.util.FirebaseAnalyticsUtil.logEvent;


public class MaintenanceActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtiy_under_maintenance);

        Bundle bundle = new Bundle();
        bundle.putString(PARAM_USER_DEVICE, PrefsUtils.getInstance().getSerialDevice());
        logEvent(ACTIVITY_MAINTENANCE, bundle);

        Button btnBackLater = findViewById(R.id.btn_back_later);
        btnBackLater.setOnClickListener(view -> finish());
    }
}