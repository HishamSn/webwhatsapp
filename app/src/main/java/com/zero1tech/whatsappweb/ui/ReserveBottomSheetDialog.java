package com.zero1tech.whatsappweb.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.zero1tech.whatsappweb.R;
import com.zero1tech.whatsappweb.data.prefs.PrefsUtils;
import com.zero1tech.whatsappweb.model.ReportModel;

import static com.zero1tech.whatsappweb.constant.FirebaseConstant.TABLE_REPORTS;


public class ReserveBottomSheetDialog extends BottomSheetDialogFragment {

    AppCompatEditText etReportDesc;
    AppCompatButton btnReportSubmit;
    AppCompatButton btnClose;
    private DatabaseReference databaseReference;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_report, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etReportDesc = view.findViewById(R.id.et_report_desc);
        btnReportSubmit = view.findViewById(R.id.btn_report_submit);
        btnClose = view.findViewById(R.id.btn_close);
        btnReportSubmit.setOnClickListener(view12 -> submitButton());
        btnClose.setOnClickListener(view1 -> dismiss());
        init();
    }


    private void init() {
        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    private void submitButton() {
        if (etReportDesc.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), "Fill Desc", Toast.LENGTH_LONG).show();
            return;
        }

        databaseReference.child(TABLE_REPORTS).push()
                .setValue(new ReportModel(etReportDesc.getText().toString(), PrefsUtils.getInstance().getSerialDevice()));
        dismiss();
        Toast.makeText(getActivity(), "Report Success, Thank you", Toast.LENGTH_LONG).show();

    }
}
