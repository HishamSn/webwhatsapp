package com.zero1tech.whatsappweb.ui.setting;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.NotificationCompat;

import com.zero1tech.whatsappweb.BuildConfig;
import com.zero1tech.whatsappweb.R;
import com.zero1tech.whatsappweb.data.prefs.PrefsUtils;
import com.zero1tech.whatsappweb.model.SettingModel;
import com.zero1tech.whatsappweb.repo.SettingRepo;
import com.zero1tech.whatsappweb.ui.BaseActivity;
import com.zero1tech.whatsappweb.ui.WebviewActivity;
import com.zero1tech.whatsappweb.util.DialogUtil;
import com.zero1tech.whatsappweb.util.NotificationUpdate;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.zero1tech.whatsappweb.constant.FirebaseConstant.ACTIVITY_UPDATE;
import static com.zero1tech.whatsappweb.constant.FirebaseConstant.PARAM_USER_DEVICE;
import static com.zero1tech.whatsappweb.util.FirebaseAnalyticsUtil.logEvent;


public class UpdateAppActivity extends BaseActivity {

    @BindView(R.id.tv_current_version)
    AppCompatTextView tvCurrentVersion;
    @BindView(R.id.tv_new_version)
    AppCompatTextView tvNewVersion;
    @BindView(R.id.btn_skip)
    AppCompatButton btnSkip;
    @BindView(R.id.btn_download)
    AppCompatButton btnDownload;
    @BindView(R.id.tv_not_release)
    AppCompatTextView tvNotRelease;

    private SettingModel settingModel;
    private SweetAlertDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtiy_update);
        ButterKnife.bind(this);

        init();
        progressDialog.show();
        SettingRepo.getInstance().getSettingEventListener(settingModel -> {
            progressDialog.dismiss();
            this.settingModel = settingModel;
            tvNewVersion.setText(appendToTextView(tvNewVersion, settingModel.getVersionName()));
            btnSkip.setVisibility(settingModel.isSkipUpdate() ? View.VISIBLE : View.GONE);
        });
        showNotification();
        tvCurrentVersion.setText(appendToTextView(tvCurrentVersion, BuildConfig.VERSION_NAME));

        if (!isReleaseEnv()) {
            tvNotRelease.setVisibility(View.VISIBLE);
            btnDownload.setVisibility(View.GONE);
        }


        Bundle bundle = new Bundle();
        bundle.putString(PARAM_USER_DEVICE, PrefsUtils.getInstance().getSerialDevice());
        logEvent(ACTIVITY_UPDATE, bundle);


    }

    private void init() {
        progressDialog = DialogUtil.progress(this);

    }


    private StringBuilder appendToTextView(TextView tv, String text) {

        return new StringBuilder(tv.getText().toString()).append(text);
    }

    @OnClick({R.id.btn_download, R.id.btn_skip})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_download:
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                break;
            case R.id.btn_skip:
                Intent intent = new Intent(this, WebviewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
        }
    }


    public void showNotification() {
        NotificationUpdate notificationUpdate = new NotificationUpdate(this);
        NotificationCompat.Builder nb = notificationUpdate.getChannelNotification();
        notificationUpdate.getManager().notify(1, nb.build());
    }


}
