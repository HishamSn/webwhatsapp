package com.zero1tech.whatsappweb.ui;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

import com.zero1tech.whatsappweb.databinding.ActivityBaseMainBinding;

public class MainActivity extends AppCompatActivity {

    private static final String DEBUG_TAG = "WAWEBTOGO";
    ActivityBaseMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBaseMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        binding.btnWeb1.setOnClickListener(view -> {
            Intent myIntent = new Intent(MainActivity.this, WebviewActivity.class);
            MainActivity.this.startActivity(myIntent);

        });
    }
}