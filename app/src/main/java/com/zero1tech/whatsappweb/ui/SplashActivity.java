package com.zero1tech.whatsappweb.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatTextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.zero1tech.whatsappweb.BuildConfig;
import com.zero1tech.whatsappweb.R;
import com.zero1tech.whatsappweb.model.SettingModel;
import com.zero1tech.whatsappweb.repo.SettingRepo;
import com.zero1tech.whatsappweb.ui.setting.MaintenanceActivity;
import com.zero1tech.whatsappweb.ui.setting.UpdateAppActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.tv_version)
    AppCompatTextView tvVersion;
    @BindView(R.id.iv_logo)
    ImageView ivLogo;


    private SettingModel settingModel;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        tvVersion.setText("V " + BuildConfig.VERSION_NAME);
        handler = new Handler();
        loadLogo();

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        SettingRepo.getInstance().getSettingEventListener(settingModel -> {
            this.settingModel = settingModel;
        });

        setHandler(2);
    }


    private void loadLogo() {

        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.ic_logo);

        Glide.with(this)
                .load("")
                .apply(requestOptions)
                .into(ivLogo);

    }

    private void setHandler(int secondsDelayed) {
        handler.postDelayed(() -> {

            if (settingModel != null && settingModel.isShutdown()) {
                startActivity(new Intent(this, MaintenanceActivity.class));
                finish();
                return;
            }

            if (settingModel != null && settingModel.getMinVersionCode() > BuildConfig.VERSION_CODE) {
                startActivity(new Intent(this, UpdateAppActivity.class));
                finish();
                return;
            }

            if (settingModel != null && settingModel.getVersionCode() > BuildConfig.VERSION_CODE && settingModel.isForceUpdate()) {
                startActivity(new Intent(this, UpdateAppActivity.class));
                finish();
                return;
            }

            Intent intent = new Intent(this, WebviewActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();

        }, secondsDelayed * 1000);
    }



}
