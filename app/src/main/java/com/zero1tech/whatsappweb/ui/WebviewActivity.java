package com.zero1tech.whatsappweb.ui;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.ValueCallback;
import android.webkit.WebStorage;
import android.webkit.WebView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.ads.AdView;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.zero1tech.whatsappweb.R;
import com.zero1tech.whatsappweb.data.prefs.PrefsUtils;
import com.zero1tech.whatsappweb.util.CustomWebChromeClient;
import com.zero1tech.whatsappweb.util.CustomWebViewClient;

import java.util.Arrays;

import static com.zero1tech.whatsappweb.constant.AppConstant.AUDIO_PERMISSION_RESULTCODE;
import static com.zero1tech.whatsappweb.constant.AppConstant.CAMERA_PERMISSION_RESULTCODE;
import static com.zero1tech.whatsappweb.constant.AppConstant.DEBUG_TAG;
import static com.zero1tech.whatsappweb.constant.AppConstant.FILECHOOSER_RESULTCODE;
import static com.zero1tech.whatsappweb.constant.AppConstant.STORAGE_PERMISSION_RESULTCODE;
import static com.zero1tech.whatsappweb.constant.AppConstant.USER_AGENT;
import static com.zero1tech.whatsappweb.constant.AppConstant.VIDEO_PERMISSION_RESULTCODE;
import static com.zero1tech.whatsappweb.constant.AppConstant.WHATSAPP_WEB_URL;
import static com.zero1tech.whatsappweb.util.AdsUtil.initBannerAd;
import static com.zero1tech.whatsappweb.util.FirebaseAnalyticsUtil.logEventWithItemId;
import static com.zero1tech.whatsappweb.util.SerialDeviceUtil.getSerialNumber;
import static com.zero1tech.whatsappweb.util.webViewUtil.configureSetting;

public class WebviewActivity extends BaseActivity {

    private final Activity activity = this;

    private WebView mWebView;
    private ViewGroup mMainView;

    private long mLastBackClick = 0;

    private CustomWebChromeClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        BuildConfig.BANNER_ID
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_whats);
        Toolbar toolbar = findViewById(R.id.toolbar);
        initAds(this);

        storeSerialNumber();


        mMainView = findViewById(R.id.layout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            try {
                setdirCache();
            } catch (Exception exception) {
                FirebaseCrashlytics.getInstance().recordException(exception);
            }
        }
        mWebView = findViewById(R.id.webview);

        // webview stuff
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        configureSetting(mWebView, USER_AGENT);


        client = new CustomWebChromeClient(this);
        mWebView.setWebChromeClient(client);

        mWebView.setWebViewClient(new CustomWebViewClient(this));

        if (savedInstanceState == null) {
            loadWhatsapp();
        } else {
            Log.d(DEBUG_TAG, "savedInstanceState is present");
        }
    }

    private void storeSerialNumber() {
        if (PrefsUtils.getInstance().getSerialDevice().equals("unknown")) {
            PrefsUtils.getInstance().setSerialDevice(getSerialNumber());
            logEventWithItemId("NEW_USER");
        }
        FirebaseCrashlytics.getInstance().setUserId(PrefsUtils.getInstance().getSerialDevice());
    }


    private void initAds(Context ctx) {
        AdView adView = new AdView(ctx);
        initBannerAd(adView, findViewById(R.id.container_ad_view), "ca-app-pub-6395927735344355/9740810102");
    }


    @RequiresApi(api = Build.VERSION_CODES.P)
    private void setdirCache() {
        String process = getProcessName(this);
        if (!getPackageName().equals(process)) {
            WebView.setDataDirectorySuffix(process);
        }
    }

    public static String getProcessName(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo processInfo : manager.getRunningAppProcesses()) {
            if (processInfo.pid == android.os.Process.myPid()) {
                return processInfo.processName;
            }
        }

        return null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
//        setNavbarEnabled(PrefsUtils.getInstance().isNavbarEnabled());

    }

    @Override
    protected void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar, menu);

        SwitchCompat switchKeyboardEnable = menu.findItem(R.id.action_switch_enable_keyboard)
                .getActionView().findViewById(R.id.switch_keyboard_enable);

        switchKeyboardEnable.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            setKeyboardEnabled(isChecked);
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_logout:
//                showToast("scroll left");
//                runOnUiThread(() -> mWebView.scrollTo(0, 0));
                logEventWithItemId("action_logout");
                showSnackbar("logging out...");
                mWebView.loadUrl("javascript:localStorage.clear()");
                WebStorage.getInstance().deleteAllData();
                loadWhatsapp();
                break;

            case R.id.menu_report:
                BottomSheetDialogFragment dialog = new ReserveBottomSheetDialog();
                dialog.show(getSupportFragmentManager(), "Dialog");
                break;

            case R.id.menu_deep_whats:

//                final String appPackageName = "com.zero1tech.whats.tools"; // getPackageName() from Context or Activity object
//                try {
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                } catch (android.content.ActivityNotFoundException anfe) {
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                }

                break;
            case R.id.menu_reload:
                logEventWithItemId("action_reload");

//                showToast("scroll right");
//                runOnUiThread(() -> mWebView.scrollTo(2000, 0));
//                mWebView.setInitialScale(50);

                showSnackbar("reloading...");
                loadWhatsapp();
                break;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case VIDEO_PERMISSION_RESULTCODE:
                if (permissions.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        client.getPermissionRequest().grant(client.getPermissionRequest().getResources());
                    } catch (RuntimeException e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                } else {
                    showSnackbar("Permission not granted, can't use video.");
                    client.getPermissionRequest().deny();
                }
                break;
            case CAMERA_PERMISSION_RESULTCODE:
            case AUDIO_PERMISSION_RESULTCODE:
                //same same
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        client.getPermissionRequest().grant(client.getPermissionRequest().getResources());
                    } catch (RuntimeException e) {
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                } else {
                    showSnackbar("Permission not granted, can't use " +
                            (requestCode == CAMERA_PERMISSION_RESULTCODE ? "camera" : "microphone"));
                    client.getPermissionRequest().deny();
                }
                break;
            case STORAGE_PERMISSION_RESULTCODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // TODO: check for current download and enqueue it
                } else {
                    showSnackbar("Permission not granted, can't download to storage");
                }
                break;
            default:
                Log.d(DEBUG_TAG, "Got permission result with unknown request code " +
                        requestCode + " - " + Arrays.asList(permissions).toString());
        }
        client.setPermissionRequest(null);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mWebView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mWebView.restoreState(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILECHOOSER_RESULTCODE) {
            FirebaseCrashlytics.getInstance().setCustomKey("resultCode", resultCode);

            if (resultCode == RESULT_CANCELED || data.getData() == null) {
                try {
                    client.getUploadMessage().onReceiveValue(null);

                } catch (Exception e) {
                    FirebaseCrashlytics.getInstance().recordException(e);
                }
            } else {
                FirebaseCrashlytics.getInstance().setCustomKey("data", "" + data.getData());
                try {
                    Uri result = data.getData();
                    Uri[] results = new Uri[1];
                    results[0] = result;
                    client.getUploadMessage().onReceiveValue(results);
                } catch (Exception e) {
                    FirebaseCrashlytics.getInstance().recordException(e);
                }
            }
        } else {
            Log.d(DEBUG_TAG, "Got activity result with unknown request code " +
                    requestCode + " - " + data.toString());
        }
    }

    private void toggleKeyboard() {
        setKeyboardEnabled(!PrefsUtils.getInstance().isKeyboardEnabled());
    }

    private void setKeyboardEnabled(final boolean enable) {
        logEventWithItemId("action_reload");

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (enable && mMainView.getDescendantFocusability() == ViewGroup.FOCUS_BLOCK_DESCENDANTS) {
            mMainView.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
            showSnackbar("Unblocking keyboard...");
            //inputMethodManager.showSoftInputFromInputMethod(activity.getCurrentFocus().getWindowToken(), 0);
        } else if (!enable) {
            mMainView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
            mWebView.getRootView().requestFocus();
            showSnackbar("Blocking keyboard...");
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
        PrefsUtils.getInstance().setKeyboardEnabled(enable);
    }

//    private void setNavbarEnabled(boolean enable) {
//        ActionBar navbar = getSupportActionBar();
//        if (navbar != null) {
//            if (enable) {
//                navbar.show();
//            } else {
//                navbar.hide();
//            }
//            PrefsUtils.getInstance().setNavbarEnabled(enable);
//        }
//    }

    @Override
    public void onBackPressed() {
        //close drawer if open and impl. press back again to leave
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (System.currentTimeMillis() - mLastBackClick < 1100) {
            finishAffinity();
        } else {
            mWebView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ESCAPE));
            showToast("Click back again to close");
            mLastBackClick = System.currentTimeMillis();
        }
    }

    private void loadWhatsapp() {
        mWebView.getSettings().setUserAgentString(USER_AGENT);
        mWebView.loadUrl(WHATSAPP_WEB_URL);
    }

//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_hide) {
//            MenuItem view = findViewById(R.id.nav_hide);
//            if (getSupportActionBar().isShowing()) {
//                showSnackbar("hiding... swipe right to show navigation bar");
//                getSupportActionBar().hide();
//            } else {
//                getSupportActionBar().show();
//            }
//        } else if (id == R.id.menu_logout) {
//            showSnackbar("logging out...");
//            mWebView.loadUrl("javascript:localStorage.clear()");
//            WebStorage.getInstance().deleteAllData();
//            loadWhatsapp();
//        } else if (id == R.id.nav_new) {
//            //showToast("nav_new");
//        } else if (id == R.id.nav_switch) {
//            //showToast("nav_switch");
//        } else if (id == R.id.nav_settings) {
//            //showToast("nav_settings");
//        } else if (id == R.id.nav_about) {
//            showDialogInfo(this, R.string.about);
//        } else if (id == R.id.menu_reload) {
//            showSnackbar("reloading...");
//            loadWhatsapp();
//        }
//
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }
}
