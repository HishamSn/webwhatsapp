package com.zero1tech.whatsappweb.repo;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.zero1tech.whatsappweb.data.prefs.PrefsUtils;
import com.zero1tech.whatsappweb.model.User;
import com.zero1tech.whatsappweb.model.track.TrackScreen;
import com.zero1tech.whatsappweb.util.DateUtil;

import java.util.Date;


public class TrackUserRepo implements IRepository<User> {

    public static final String TABLE_USERS = "Users";
    public static final String TABLE_TRACK = "Track";
    public static final String TABLE_ACTIVITY = "Activity";

    private static TrackUserRepo instance;
    DatabaseReference databaseReference;

    TrackUserRepo() {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(TABLE_USERS)
                .child(PrefsUtils.getInstance().getAndroidId()).child(TABLE_TRACK);
    }

    public static synchronized TrackUserRepo getInstance() {
        if (instance == null) {
            instance = new TrackUserRepo();
        }
        return instance;
    }


    public void insertTrackActivity(String activityName, TrackScreen trackScreen, FunctionCallBack<Boolean> function) {

        Date date = new Date();
        String dateFormat = DateUtil.getSimpleDateFormat().format(date);


        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference()
                .child(TABLE_USERS).child(PrefsUtils.getInstance().getAndroidId())
                .child(TABLE_TRACK).child(TABLE_ACTIVITY)
                .child(activityName).child(dateFormat);


        databaseReference.push().setValue(trackScreen).addOnCompleteListener(task -> {
            function.done(true);
        });


    }


//    public void insertTrackActivity(String activityName, TrackActivity trackActivity, Function<Boolean> function) {
//
//        Date date = new Date();
//        String dateFormat = DateUtil.getSimpleDateFormat().format(date);
//
//
//        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference()
//                .child(TABLE_USERS).child(PrefsUtils.getInstance().getAndroidId())
//                .child(TABLE_TRACK).child(TABLE_ACTIVITY)
//                .child(activityName).child(dateFormat);
//
//
//        databaseReference.push().setValue(trackActivity.getTrackScreens()).addOnCompleteListener(task -> {
//            function.done(true);
//        });
//
//
//    }


}

