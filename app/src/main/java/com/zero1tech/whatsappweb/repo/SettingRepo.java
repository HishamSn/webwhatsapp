package com.zero1tech.whatsappweb.repo;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.zero1tech.whatsappweb.model.SettingModel;

import static com.zero1tech.whatsappweb.constant.FirebaseConstant.TABLE_SETTING;


public class SettingRepo {


    private static SettingRepo instance;
    DatabaseReference databaseReference;

    SettingRepo() {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(TABLE_SETTING);
//   databaseReference.keepSynced(true);
    }

    public static synchronized SettingRepo getInstance() {
        if (instance == null) {
            instance = new SettingRepo();
        }
        return instance;
    }


    public void getSettingEventListener(FunctionCallBack<SettingModel> functionCallBack) {

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child(TABLE_SETTING);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                SettingModel settingModel = dataSnapshot.getValue(SettingModel.class);
                functionCallBack.done(settingModel);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
