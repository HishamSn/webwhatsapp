package com.zero1tech.whatsappweb.repo;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.zero1tech.whatsappweb.data.prefs.PrefsUtils;
import com.zero1tech.whatsappweb.model.ErrorAds;
import com.zero1tech.whatsappweb.util.DateUtil;

import java.util.Date;

import static com.zero1tech.whatsappweb.constant.FirebaseConstant.TABLE_ADS;



public class AdsUserRepo  {


    private static AdsUserRepo instance;
    DatabaseReference databaseReference;

    AdsUserRepo() {
        databaseReference = FirebaseDatabase.getInstance().getReference().child(TABLE_ADS);

    }

    public static synchronized AdsUserRepo getInstance() {
        if (instance == null) {
            instance = new AdsUserRepo();
        }
        return instance;
    }


    public void insertErrorAdsActivity( ErrorAds errorAds, FunctionCallBack<Boolean> function) {

        Date date = new Date();
        String dateFormat = DateUtil.getSimpleDateFormat().format(date);


        databaseReference.child(PrefsUtils.getInstance().getAndroidId())
                .child(dateFormat).push().setValue(errorAds).addOnCompleteListener(task -> {
            function.done(true);
        });


    }


}

