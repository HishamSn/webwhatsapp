package com.zero1tech.whatsappweb.repo;


public interface FunctionCallBack<T> {
    void done(T t);
}
