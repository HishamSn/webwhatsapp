package com.zero1tech.whatsappweb.common;

import android.app.Application;
import android.util.Log;

import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;


public class MyApplication extends Application {
    private FirebaseAnalytics FirebaseAnalytics;

    private static MyApplication instance;
//    private FirebaseAnalytics FirebaseAnalytics;

    public static MyApplication getInstance() {
        return instance;
    }



    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
//        LocalHelper.onAttach(this,"es");
//        Pandora.get().getInterceptor();
//        FirebaseAnalytics = FirebaseAnalytics.getInstance(MyApplication.getInstance());
//
//        MobileAds.initialize(this, BuildConfig.AD_MOB_ID);

        MobileAds.initialize(this, initializationStatus ->
                Log.d("MyApplication",initializationStatus.toString()));

        FirebaseAnalytics = FirebaseAnalytics.getInstance(MyApplication.getInstance());

        registerActivityLifecycleCallbacks(new LifecycleCallbacks());
    }


    public FirebaseAnalytics getFirebaseAnalytics() {
        return FirebaseAnalytics;
    }

}