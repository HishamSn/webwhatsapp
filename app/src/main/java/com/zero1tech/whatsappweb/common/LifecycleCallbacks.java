package com.zero1tech.whatsappweb.common;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import com.zero1tech.whatsappweb.model.track.TrackActivity;
import com.zero1tech.whatsappweb.model.track.TrackScreen;
import com.zero1tech.whatsappweb.repo.TrackUserRepo;

import java.util.ArrayList;
import java.util.List;

public final class LifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
    private static final String Application = "App";
    TrackScreen trackScreen = new TrackScreen();

    private int activityReferences = 0;
    private boolean isActivityChangingConfigurations = false;
    private long startingTimeApp;

    private List<TrackActivity> trackActivityList = new ArrayList<>();

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        Log.i("Tracking_Activity", "Created : " + activity.getLocalClassName());
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.i("Tracking_Activity", "Started : " + activity.getLocalClassName());

        trackAppOnStart();
        trackActivitiesOnStart(activity);

    }


    @Override
    public void onActivityResumed(Activity activity) {
//            Adjust.onResume();
        Log.i("Tracking_Activity", "Resumed : " + activity.getClass().getSimpleName());

    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.i("Tracking_Activity", "Paused : " + activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.i("Tracking_Activity", "Stopped : " + activity.getClass().getSimpleName());

        trackAppOnStop(activity);
        trackActivitiesOnStop(activity);

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Log.i("Tracking_Activity", "SaveInstanceState : " + activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.i("Tracking_Activity", "Destroyed : " + activity.getClass().getSimpleName());

    }


    private void trackAppOnStart() {
        if (++activityReferences == 1 && !isActivityChangingConfigurations) {
            startingTimeApp = getCurrentTime(); //This is the starting time when the app began to start
            trackScreen.setStartTime(startingTimeApp);
        }
    }


    private void trackActivitiesOnStart(Activity activity) {
        long startingTimeActivity = getCurrentTime();
        if (!isActivityTracked(activity.getClass().getSimpleName())) {
            TrackScreen trackTemp = new TrackScreen();
            trackTemp.setStartTime(startingTimeActivity);
            trackActivityList.add(new TrackActivity(activity.getClass().getSimpleName(), trackTemp));
        }
    }


    private void trackActivitiesOnStop(Activity activity) {
        long stopTimeActivity = getCurrentTime();
        int activityIndex = getActivityIndex(activity.getClass().getSimpleName());

        if (activityIndex != -1) {
            TrackScreen trackScreen2 = trackActivityList.get(activityIndex).getTrackScreens();
            trackScreen2.setEndTime(stopTimeActivity);
            trackScreen2.setTotalSpentTime(trackScreen2.getEndTime() - trackScreen2.getStartTime());

            if (isActivityTracked(activity.getClass().getSimpleName())) {
                TrackUserRepo.getInstance().insertTrackActivity(
                        trackActivityList.get(activityIndex).getName(), trackScreen2, d -> {

                        });
                trackActivityList.remove(activityIndex);
            }

        }
    }

    private void trackAppOnStop(Activity activity) {
        isActivityChangingConfigurations = activity.isChangingConfigurations();
        if (--activityReferences == 0 && !isActivityChangingConfigurations) {

            long stopTimeApp = getCurrentTime();//This is the ending time when the app is stopped

            long totalSpentTimeApp = stopTimeApp - startingTimeApp; //This is the total spent time of the app in foreground
            trackScreen.setEndTime(stopTimeApp);
            trackScreen.setTotalSpentTime(totalSpentTimeApp);
            TrackUserRepo.getInstance().insertTrackActivity(Application, trackScreen, d -> {

            });

        }
    }


    private long getCurrentTime() {
        return System.currentTimeMillis() / 1000;
    }


    public boolean isActivityTracked(String activityName) {
        for (int i = 0; i < trackActivityList.size(); i++) {
            if (trackActivityList.get(i).getName().equals(activityName)) {
                return true;
            }
        }
        return false;
    }

    public int getActivityIndex(String activityName) {
        for (int i = 0; i < trackActivityList.size(); i++) {
            if (trackActivityList.get(i).getName().equals(activityName)) {
                return i;
            }
        }
        return -1;
    }

}