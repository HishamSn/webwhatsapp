package com.zero1tech.whatsappweb.model;

public class ErrorAds {

    private String createdAt;
    private String domain;
    private String message;
    private String ResponseInfo;
    private int code;

    public ErrorAds() {
    }

    public ErrorAds(String createdAt, String domain, String message, String responseInfo, int code) {
        this.createdAt = createdAt;
        this.domain = domain;
        this.message = message;
        ResponseInfo = responseInfo;
        this.code = code;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponseInfo() {
        return ResponseInfo;
    }

    public void setResponseInfo(String responseInfo) {
        ResponseInfo = responseInfo;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
