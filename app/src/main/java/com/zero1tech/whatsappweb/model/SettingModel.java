package com.zero1tech.whatsappweb.model;

public class SettingModel {

    private Integer versionCode;
    private Integer minVersionCode;
    private String urlLatestVersionApp;
    private String urlLatestVersionAppStage;
    private String versionName;
    private boolean forceUpdate;
    private boolean shutdown;
    private boolean skipUpdate;

    public Integer getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public Integer getMinVersionCode() {
        return minVersionCode;
    }

    public void setMinVersionCode(Integer minVersionCode) {
        this.minVersionCode = minVersionCode;
    }

    public String getUrlLatestVersionApp() {
        return urlLatestVersionApp;
    }

    public void setUrlLatestVersionApp(String urlLatestVersionApp) {
        this.urlLatestVersionApp = urlLatestVersionApp;
    }

    public boolean isForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public boolean isShutdown() {
        return shutdown;
    }

    public void setShutdown(boolean shutdown) {
        this.shutdown = shutdown;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getUrlLatestVersionAppStage() {
        return urlLatestVersionAppStage;
    }

    public void setUrlLatestVersionAppStage(String urlLatestVersionAppStage) {
        this.urlLatestVersionAppStage = urlLatestVersionAppStage;
    }

    public boolean isSkipUpdate() {
        return skipUpdate;
    }

    public void setSkipUpdate(boolean skipUpdate) {
        this.skipUpdate = skipUpdate;
    }
}
