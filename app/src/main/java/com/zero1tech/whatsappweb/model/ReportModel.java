package com.zero1tech.whatsappweb.model;

public class ReportModel {

    private String desc;
    private String serialDevice;

    public ReportModel() {
    }

    public ReportModel(String desc, String serialDevice) {
        this.desc = desc;
        this.serialDevice = serialDevice;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getSerialDevice() {
        return serialDevice;
    }

    public void setSerialDevice(String serialDevice) {
        this.serialDevice = serialDevice;
    }
}
