package com.zero1tech.whatsappweb.model.track;

public class TrackScreen {

    private Long startTime;
    private Long endTime;
    private Long totalSpentTime;

    public TrackScreen() {
    }

    public TrackScreen(Long startTime, Long endTime, Long totalSpentTime) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.totalSpentTime = totalSpentTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getTotalSpentTime() {
        return totalSpentTime;
    }

    public void setTotalSpentTime(Long totalSpentTime) {
        this.totalSpentTime = totalSpentTime;
    }
}
