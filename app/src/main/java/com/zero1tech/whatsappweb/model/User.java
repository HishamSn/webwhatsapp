package com.zero1tech.whatsappweb.model;

public class User {

    private String serialDevice;
    private int numLogout;
    private int numLogin;


    public String getSerialDevice() {
        return serialDevice;
    }

    public void setSerialDevice(String serialDevice) {
        this.serialDevice = serialDevice;
    }

    public int getNumLogout() {
        return numLogout;
    }

    public void setNumLogout(int numLogout) {
        this.numLogout = numLogout;
    }

    public int getNumLogin() {
        return numLogin;
    }

    public void setNumLogin(int numLogin) {
        this.numLogin = numLogin;
    }
}
