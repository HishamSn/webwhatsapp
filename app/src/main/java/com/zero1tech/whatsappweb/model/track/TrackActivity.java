package com.zero1tech.whatsappweb.model.track;

public class TrackActivity {
    private String name;
    private TrackScreen trackScreens;

    public TrackActivity() {
    }

    public TrackActivity(String name, TrackScreen trackScreens) {
        this.name = name;
        this.trackScreens = trackScreens;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public TrackScreen getTrackScreens() {
        return trackScreens;
    }

    public void setTrackScreens(TrackScreen trackScreens) {
        this.trackScreens = trackScreens;
    }
}
