package com.zero1tech.whatsappweb.constant;

public class FirebaseConstant {

    public static final String TABLE_REPORTS = "NewReports";

    public static final String PARAM_USER_DEVICE = "user_device";

    public static final String ACTIVITY_UPDATE = "activity_update_create";
    public static final String ACTIVITY_MAINTENANCE = "activity_maintenance_create";
    public static final String TABLE_SETTING = "Setting";

    public static final String TABLE_ADS = "ADS_REPORT";

}
