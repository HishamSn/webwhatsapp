package com.zero1tech.whatsappweb.constant;

import android.Manifest;

import java.util.Locale;

public final class AppConstant {

    public static final String WEB_WHATSAPP_URL = "web.whatsapp.com";

    public static final String CHROME_FULL = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36";
    public static final String USER_AGENT = CHROME_FULL;
    public static final String USER_AGENT2 = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36";

    public static final String CAMERA_PERMISSION = Manifest.permission.CAMERA; // "android.permission.CAMERA";
    public static final String AUDIO_PERMISSION = Manifest.permission.RECORD_AUDIO; // "android.permission.RECORD_AUDIO";
    public static final String[] VIDEO_PERMISSION = {CAMERA_PERMISSION, AUDIO_PERMISSION};

    public static final String WORLD_ICON = "\uD83C\uDF10";
    public static final String WHATSAPP_WEB_URL = "https://web.whatsapp.com"
            + "/" + WORLD_ICON + "/"
            + Locale.getDefault().getLanguage();



    public static final int FILECHOOSER_RESULTCODE        = 200;
    public static final int CAMERA_PERMISSION_RESULTCODE  = 201;
    public static final int AUDIO_PERMISSION_RESULTCODE   = 202;
    public static final int VIDEO_PERMISSION_RESULTCODE   = 203;
    public static final int STORAGE_PERMISSION_RESULTCODE = 204;

    public static final String DEBUG_TAG = "WAWEBTOGO";




}
